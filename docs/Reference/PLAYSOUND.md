# PLAYSOUND

| 関数名                                                         | 引数     | 戻り値 |
| :------------------------------------------------------------- | :------- | :----- |
| ![](../assets/images/IconEE.webp)[`PLAYSOUND`](./PLAYSOUND.md) | `string` | `void` |

!!! info "API"

	``` { #language-erbapi }
	PLAYSOUND MediaFile
	```

	`sound`フォルダ内にある音声ファイルを1回再生する

!!! hint "ヒント"

    命令としてのみ使用可能  
	最大10個の音声ファイルを同時再生可能  
	WMPLibを使用しているため、WMPで再生できる形式に対応
