# PLAYBGM

| 関数名                                                     | 引数     | 戻り値 |
| :--------------------------------------------------------- | :------- | :----- |
| ![](../assets/images/IconEE.webp)[`PLAYBGM`](./PLAYBGM.md) | `string` | `void` |

!!! info "API"

	``` { #language-erbapi }
	PLAYBGM MediaFile
	```

	`sound`フォルダ内にある音声ファイルをループ再生する

!!! hint "ヒント"

    命令としてのみ使用可能  
	最大10個の音声ファイルを同時再生可能  
	WMPLibを使用しているため、WMPで再生できる形式に対応
