# FORCE_QUIT

| 函数名                                                           | 参数   | 返回值 |
| :--------------------------------------------------------------- | :----- | :----- |
| ![](../assets/images/IconEE.webp)[`FORCE_QUIT`](./FORCE_QUIT.md) | `void` | `void` |

!!! info "API"

	``` { #language-erbapi }
	FORCE_QUIT
	```

	未附加 `WAIT` 的 `QUIT`。（即直接退出）

!!! hint "提示"

    只能作为命令使用。
