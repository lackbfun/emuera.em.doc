# SETBGMVOLUME

| 関数名                                                               | 引数  | 戻り値 |
| :------------------------------------------------------------------- | :---- | :----- |
| ![](../assets/images/IconEE.webp)[`SETBGMVOLUME`](./SETBGMVOLUME.md) | `int` | `void` |

!!! info "API"

	``` { #language-erbapi }
	SETBGMVOLUME int(0～100)
	```

	`PLAYBGM`の音量を0～100の値で設定する

!!! hint "ヒント"

    命令としてのみ使用可能
