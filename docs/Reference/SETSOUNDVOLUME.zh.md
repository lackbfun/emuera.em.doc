# SETSOUNDVOLUME

| 函数名                                                                   | 参数  | 返回值 |
| :----------------------------------------------------------------------- | :---- | :----- |
| ![](../assets/images/IconEE.webp)[`SETSOUNDVOLUME`](./SETSOUNDVOLUME.md) | `int` | `void` |

!!! info "API"

    ``` { #language-erbapi }
    SETSOUNDVOLUME int(0～100)
    ```

    将 `PLAYSOUND` 的音量设置为 `0` ～ `100` 之间。

!!! hint "提示"

    只能作为命令使用。
