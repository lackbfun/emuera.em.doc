# STOPSOUND

| 函数名                                                         | 参数   | 返回值 |
| :------------------------------------------------------------- | :----- | :----- |
| ![](../assets/images/IconEE.webp)[`STOPSOUND`](./STOPSOUND.md) | `void` | `void` |

!!! info "API"

    ``` { #language-erbapi }
    STOPSOUND
    ```

    停止 `PLAYSOUND` 正在播放的音轨。

!!! hint "提示"

    只能作为命令使用。
