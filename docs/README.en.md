---
hide:
  - navigation
---
# Welcome


## Contributions welcome
> Feel free to translate and contribute to this document!

## Licence
> This document is licensed under the [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/deed.en) license.

[![CC BY-NC 4.0](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)](https://creativecommons.org/licenses/by-nc/4.0/deed.en)