---
hide:
  - navigation
---

# 欢迎

## EmueraEM+EE 是什么

「EmueraEM+EE」是基于「[Emuera 私家版](https://ux.getuploader.com/ninnohito/)」改进增强的新版 Emuera。

### Emuera 私家版

!!! quote "引用"

    由于「Emuera 本家」已停止更新，「私家改造版」是其他人接手后继续维护（只修复本家的 bug，不添加任何新功能）的版本。

## 授权许可

> 请在 [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/deed.zh) 的许可范围内使用本文档。

[![CC BY-NC 4.0](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)](https://creativecommons.org/licenses/by-nc/4.0/deed.zh)
